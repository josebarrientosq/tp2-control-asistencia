# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import werkzeug.utils
from odoo.addons.web.controllers.main import Database
import unicodedata
import os
import pytz
from odoo.exceptions import ValidationError, UserError
from odoo import models, fields, api,_

class control_empleados(http.Controller):

    fecha_inicio= None
    fecha_fin = None
    nombre = None
    dni=None

    @http.route('/control_empleados', auth='public')
    def login(self, **kwargs):
        return request.render('rekognition.login')



    @http.route('/webcam', auth='public')
    def captura(self, **kwargs):
        return request.render('rekognition.webcam')

    @http.route('/autenticador', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def autenticar(self, **post):
        dni = post.get("dni")
        password = post.get("password")

        empleado= request.env["hr.employee"].sudo().search([('identification_id','=',dni)])

        if empleado:

            nombre = empleado.name
            if empleado.clave == password:
                return http.request.render('rekognition.principal', {'empleado' : empleado , 'nombre': nombre, 'dni': dni})
            else:
                return 'clave'
        else:
            return 'dni'


    @http.route('/marcacion', auth='public')
    def marcacion(self, **kwargs):
        return request.render('rekognition.marcacion')

    @http.route('/marcar_asistencia', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def marcar_asistencia(self, **post):
        marca = post.get("marca")
        marca_local = self.convert_time_to_utc(marca)

        dni = post.get("dni")
        zona = post.get("zona")

        zona_id= request.env["asistencia.zona"].sudo().search([('name', '=', zona)])

        empleado= request.env["hr.employee"].sudo().search([('identification_id','=',dni)])

        estado = 'NOAUTORIZADO' \
                 ''
        for emp in zona_id.empleados_ids:
            if emp.name == empleado.name:
                estado = 'AUTORIZADO'

        marca_obj = request.env["asistencia.marca"].sudo().create({
            'marca' : marca_local,
            'employee_id' : empleado.id,
            'zona_id' : zona_id.id,
            'estado' : estado,
        })

        if empleado:
            if estado=='AUTORIZADO':
                return request.render('rekognition.alerta', {'nombre': empleado.name ,'hora': marca,'zona': zona_id.name})
            else:
                return "<div class='alert alert-danger' role='alert'>NO AUTORIZADO</div>"
        else:
            return "<div class='alert alert-danger' role='alert'>NO ESTA REGISTRADO EN EL SISTEMA</div>"


    @http.route('/asistencia/zona/<zona>', type='http', auth='public', method=["POST"], csrf=False , website=True)
    def marcar_asistencia_zona(self, zona,**post):
        zona = request.env["asistencia.zona"].sudo().search([('name', '=', zona)])
        if zona:
            return request.render('rekognition.marcacion', {'zona': zona.name})

    @http.route('/permitir_marcar', type='http', auth='public', method=["POST"], csrf=False, website=True)
    def permitir_marcar_asistencia(self, **post):

        return request.render('rekognition.marcacion')

    @http.route('/test', auth='public')
    def marcaciontest(self, **kwargs):
        return request.render('rekognition.test')

    @http.route('/alerta', auth='public')
    def marcaciontest(self, **kwargs):
        return request.render('rekognition.alerta')

    def convert_time_to_utc(self, datetime_str, tz_name='America/Lima'):
        """
        @param datetime_str: datetime string in Odoo datetime string format
        @param tz_name: the name of the timezone to convert. In case of no tz_name passed, this method will try to find the timezone in context or the login user record

        @return: datetime string in Odoo datetime string format
        """
        tz_name = tz_name or self._context.get('tz') or self.env.user.tz
        if not tz_name:
            raise ValidationError(_("Zona horaria no esta definida"))
        local = pytz.timezone(tz_name)
        naive = fields.Datetime.from_string(datetime_str)
        local_dt = local.localize(naive, is_dst=None)
        utc_dt = local_dt.astimezone(pytz.utc)
        return fields.Datetime.to_string(utc_dt)