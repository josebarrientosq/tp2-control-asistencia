// Inicializar el proveedor de credenciales de Amazon Cognito
AWS.config.region = 'us-east-2'; // Región
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-2:dccecf43-9cfd-4ed7-acfd-f20e908c1d2a',
});


var s3 = new AWS.S3({
  apiVersion: "2006-03-01",
  params: { Bucket: 'bucketrostros1' }
});

var rekognition_cliente = new AWS.Rekognition();

var dynamo_cliente = new AWS.DynamoDB();


Webcam.set({
  width: 480,
  height: 360,
  image_format: 'jpeg',
  jpeg_quality: 90
 });
Webcam.attach( '#my_camera' );

<!-- Code to handle taking the snapshot and displaying it locally -->
function take_snapshot() {

 // take snapshot and get image data
 Webcam.snap( function(data_uri) {
  // display results in page
  ProcessImage(data_uri)
    console.log("procesando imagen")
  } );
}


function reconocer_rostro(imageData) {
    var params = {
      CollectionId : 'colectionrostros1',
      Image: {
        Bytes: imageData
      },
    };
    rekognition_cliente.searchFacesByImage(params, function (err, data) {
      if (err){
        console.log("rostro desconocido");
        enviar_marcacion(0);
      }  // an error occurred

      if(data) {
          for (var i = 0; i < data.FaceMatches.length; i++) {
                console.log(data.FaceMatches[i].Face.FaceId);
                buscar_item(data.FaceMatches[i].Face.FaceId);
            }
      }

    });
}

function buscar_item(faceid) {
    var params = {
        Key: {
            "RekognitionId": { S: faceid }
        },
        TableName: "tablarostros1",
        };
    console.log("Buscando item")
    dynamo_cliente.getItem(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
        console.log("mano");
    if ( data != null) {

        dni = data.Item.FullName.S;
        enviar_marcacion(dni);
    }

 });
}

//Loads selected image and unencodes image bytes for Rekognition  API
function ProcessImage(image_base) {
        var image = null;
        var jpg = true;
        try {
          image = atob(image_base.split("data:image/jpeg;base64,")[1]);

        } catch (e) {
          jpg = false;
        }
        if (jpg == false) {
          try {
            image = atob(image_base.split("data:image/png;base64,")[1]);
          } catch (e) {
            alert("Not an image file Rekognition can process");
            return;
          }
        }
        //unencode image bytes for Rekognition DetectFaces API
        var length = image.length;
        imageBytes = new ArrayBuffer(length);
        var ua = new Uint8Array(imageBytes);
        for (var i = 0; i < length; i++) {
          ua[i] = image.charCodeAt(i);
        }
        //Call Rekognition
        reconocer_rostro(imageBytes);

}

function enviar_marcacion(dni){
    console.log("enviando");
    var data = {
        "marca" : $("#horacompleta").val(),
        "dni" : dni,
        "zona" : $("#zona").text(),
    }
    console.log($("#hora").val());
    console.log(dni);
    console.log($("#zona").text());

    $.post("/marcar_asistencia", data).done(function(response) {

        console.log(response)
        $("#respuesta").html(response)

    })


}


////////////////////////////////////////////////////
//TEST  https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/s3-example-photo-album-full.html
function getHtml(template) {
  return template.join('\n');
}


function listBuckets() {
s3.listBuckets(function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});
}

function listAlbums() {
    // Load the SDK para JavaScript
console.log("listar albumes");

  s3.listObjects({ Delimiter: "/" }, function(err, data) {
    if (err) {
      return alert("There was an error listing your albums: " + err.message);
    } else {
    console.log("sin errores");
      var albums = data.CommonPrefixes.map(function(commonPrefix) {
        var prefix = commonPrefix.Prefix;
        var albumName = decodeURIComponent(prefix.replace("/", ""));
        return getHtml([
          "<li>",
          "<span onclick=\"deleteAlbum('" + albumName + "')\">X</span>",
          "<span onclick=\"viewAlbum('" + albumName + "')\">",
          albumName,
          "</span>",
          "</li>"
        ]);
      });
      var message = albums.length
        ? getHtml([
            "<p>Click on an album name to view it.</p>",
            "<p>Click on the X to delete the album.</p>"
          ])
        : "<p>You do not have any albums. Please Create album.";
      var htmlTemplate = [
        "<h2>Albums</h2>",
        message,
        "<ul>",
        getHtml(albums),
        "</ul>",
        "<button onclick=\"createAlbum(prompt('Enter Album Name:'))\">",
        "Create New Album",
        "</button>"
      ];
      document.getElementById("app").innerHTML = getHtml(htmlTemplate);
    }
  });
}



//subir foto a s3
function addPhoto() {
  var albumName = "albumtest"
  var files = document.getElementById("photoupload").files;
  if (!files.length) {
    return alert("Please choose a file to upload first.");
  }
  var file = files[0];
  var fileName = file.name;
  var albumPhotosKey = encodeURIComponent(albumName) + "//";

  var photoKey = albumPhotosKey + fileName;

  // Use S3 ManagedUpload class as it supports multipart uploads
  var upload = new AWS.S3.ManagedUpload({
    params: {
      Bucket: "bucketrostros1",
      Key: 'index/'+fileName,
      Body: file,
      ACL: "public-read"
    }
  });

  var promise = upload.promise();

  promise.then(
    function(data) {
      alert("Successfully uploaded photo.");

    },
    function(err) {
      return alert("There was an error uploading your photo: ", err.message);
    }
  );
}


function analizar() {
    var control = document.getElementById("imageprev");
    var file = control.files[0];

    // Load base64 encoded image for display
    var reader = new FileReader();
    reader.onload = (function (theFile) {
      return function (e) {
        //Call Rekognition
        AWS.region = 'us-east-2';
        var rekognition = new AWS.Rekognition();
        var params = {
            CollectionId : 	'colectionrostros1',
            Image: {
            Bytes: e.target.result
            },
        };
    rekognition.searchFacesByImage(params, function(err, data) {
    if (err) {console.log(err, err.stack); }// an error occurred
    else {


        if (err) console.log(err, err.stack); // an error occurred
        else     console.log(data);

    }              // successful response
    });
};
    })(file);
    reader.readAsArrayBuffer(file);

  }

