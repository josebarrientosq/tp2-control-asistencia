# -*- coding: utf-8 -*-
{
    'name': "rekognition",

    'summary': """
        Modulo para reconocimiento facial con aws""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','resource'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/view_company.xml',
        'views/hr_employee_view.xml',
        'views/captura_rostro_frontend.xml',
        'views/webcam.xml',
        'views/asistencia_marcas_view.xml',
        'views/asistencia_zonas_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}