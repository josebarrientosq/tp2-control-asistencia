from odoo import api, fields, models ,_ ,exceptions
import boto3
import base64
import os
from odoo.exceptions import UserError

class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    name = fields.Char(related='resource_id.name', store=True, oldname='name_related')
    user_id = fields.Many2one('res.users', 'User', related='resource_id.user_id')
    active = fields.Boolean('Active', related='resource_id.active', default=True, store=True)

    # informacion del trabajo
    address_home_id = fields.Many2one(
        'res.partner', 'Private Address',
        help='Enter here the private address of the employee, not the one linked to your company.',
        groups="hr.group_hr_user")
    is_address_home_a_company = fields.Boolean(
        'The employee adress has a company linked',
        compute='_compute_is_address_home_a_company',
    )
    country_id = fields.Many2one(
        'res.country', 'Nacionalidad', groups="hr.group_hr_user")
    gender = fields.Selection([
        ('male', 'Masculino'),
        ('female', 'Femenino') ], groups="hr.group_hr_user", default="male")
    marital = fields.Selection([
        ('single', 'Soltero(a)'),
        ('married', 'Casado(a)'),
        ('cohabitant', 'Conviviente'),
        ('widower', 'Viudo(a)'),
        ('divorced', 'Divorciado(a)')
    ], string='Estado civil', groups="hr.group_hr_user", default='single')
    birthday = fields.Date('Fecha de nacimiento', groups="hr.group_hr_user")
    ssnid = fields.Char('SSN No', help='Social Security Number', groups="hr.group_hr_user")
    sinid = fields.Char('SIN No', help='Social Insurance Number', groups="hr.group_hr_user")
    identification_id = fields.Char(string='DNI', groups="hr.group_hr_user")
    passport_id = fields.Char('Passport No', groups="hr.group_hr_user")
    bank_account_id = fields.Many2one(
        'res.partner.bank', 'Bank Account Number',
        domain="[('partner_id', '=', address_home_id)]",
        groups="hr.group_hr_user",
        help='Employee bank salary account')
    permit_no = fields.Char('Permiso de trabajo No', groups="hr.group_hr_user")
    visa_no = fields.Char('Visa No', groups="hr.group_hr_user")
    visa_expire = fields.Date('Visa Expiracion', groups="hr.group_hr_user")

    # imagen

    image_small = fields.Binary(
        "Small-sized photo", attachment=True,
        help="Small-sized photo of the employee. It is automatically "
             "resized as a 64x64px image, with aspect ratio preserved. "
             "Use this field anywhere a small image is required.")

    # informacion de trabajo
    address_id = fields.Many2one(
        'res.partner', 'Dirección de trabajo')
    work_phone = fields.Char('Teléfono del trabajo')
    mobile_phone = fields.Char('Móvil del trabajo')
    work_email = fields.Char('Correo electrónico del trabajo')
    work_location = fields.Char('Ubicación del trabajo')
    # Cargo
    job_id = fields.Many2one('hr.job', 'Puesto de Empleo')
    department_id = fields.Many2one('hr.department', 'Departmento')
    parent_id = fields.Many2one('hr.employee', 'Jefe')
    child_ids = fields.One2many('hr.employee', 'parent_id', string='Subordinados')
    coach_id = fields.Many2one('hr.employee', 'Supervisor')
    category_ids = fields.Many2many('hr.employee.category', 'employee_category_rel', 'emp_id', 'category_id', string='Tags')
    # misc



    clave = fields.Char("Clave empleado")

    zona_admin_ids = fields.Many2many('asistencia.zona', 'zona_admin_rel', 'admin_id','zona_id',string='Zonas de control')
    zona_ids = fields.Many2many('asistencia.zona','zona_employee_rel', 'emp_id','zona_id',string='Zona de ingresos')



    def get_aws_session(self):
        if self.company_id.aws_access_key is None or self.company_id.aws_secret_key is None:
            raise exceptions.UserError("Configurar accesos a AWS rekognition")
        return boto3.Session(
            aws_access_key_id=self.company_id.aws_access_key,
            aws_secret_access_key=self.company_id.aws_secret_key,
            region_name=self.company_id.aws_region
        )

    def upload_image(self):

        session = self.get_aws_session()
        s3 = session.resource('s3')
        s3.Bucket(self.company_id.bucket).put_object(Key='index/'+self.identification_id+'.jpg',
                                                     Body=base64.decodestring(self.image),
                                                     ContentType='image/jpg',
                                                     ContentEncoding= 'base64',
                                                     Metadata={'FullName': self.identification_id},
                                                     ACL='public-read')

    def get_aws_client(self, service):
        if self.company_id.aws_access_key is None or self.company_id.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")

        return boto3.client(service,
                            aws_access_key_id=self.company_id.aws_access_key,
                            aws_secret_access_key=self.company_id.aws_secret_key,
                            region_name=self.company_id.aws_region)

    def analizar(self):
        rekognition_client = self.get_aws_client('rekognition')
        dynamodb_client = self.get_aws_client('dynamodb')

        #image = Image.open("images/seria.jpg")
        #stream = io.BytesIO()
        #image.save(stream, format="JPEG")
        #image_binary = stream.getvalue()
        image_binary = base64.decodestring(self.image)

        response = rekognition_client.search_faces_by_image(
            CollectionId = self.company_id.rekognition,
            Image={'Bytes': image_binary}
        )

        for match in response['FaceMatches']:
            os.system("echo '%s'" % (match['Face']['FaceId']))
            os.system("echo '%s'" % (match['Face']['Confidence']))
            #print(match['Face']['FaceId'], match['Face']['Confidence'])

            face = dynamodb_client.get_item(
                TableName= self.company_id.dynamo,
                Key={'RekognitionId': {'S': match['Face']['FaceId']}}
            )

            if 'Item' in face:
                raise UserError(face['Item']['FullName']['S'])
                os.system("echo '%s'" % (face['Item']['FullName']['S']))
                #print(face['Item']['FullName']['S'])
            else:
                os.system("echo '%s'" % ("NO HAY"))
                raise UserError("no se encuentra")
                #print('no match found in person lookup')

class ResourceMixin(models.AbstractModel):
    _inherit = "resource.mixin"

    resource_id = fields.Many2one(
        'resource.resource', 'Resource',
        auto_join=True, index=True, ondelete='restrict', required=True)
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env['res.company']._company_default_get(),
        index=True, related='resource_id.company_id', store=True)
    resource_calendar_id = fields.Many2one(
        'resource.calendar', 'Horario de trabajo',
        default=lambda self: self.env['res.company']._company_default_get().resource_calendar_id,
        index=True, related='resource_id.calendar_id')