from odoo import models, fields, api, _


class marcas(models.Model):
    _name = 'asistencia.marca'
    _description = 'Marcaciones'
    _order = 'marca DESC'

    marca = fields.Datetime(string='Marca', required=True, index=True)

    employee_id = fields.Many2one('hr.employee', string='Empleado')

    zona_id = fields.Many2one('asistencia.zona', 'Zona de control')
    estado = fields.Selection([('AUTORIZADO', 'AUTORIZADO'), ('NOAUTORIZADO', 'NO AUTORIZADO')], string='Estado')
