from odoo import models, fields, api, exceptions
import boto3
import botocore
import os
import io
from PIL import Image
from odoo.exceptions import UserError

class awsclient(models.Model):
    _name = 'aws.client.rekognition'

    name = fields.Char(string="Nombre")
    aws_access_key = fields.Char(string="AWS Access Key")
    aws_secret_key = fields.Char(string="AWS Secret Key")
    aws_region = fields.Char(string="AWS Region", default="us-east-2")

    bucket = fields.Char()
    dynamo = fields.Char()
    rekognition = fields.Char()

    imagen = fields.Binary("Favicon", attachment=True)

    def crear_cloud(self):
        cloudformation = self.get_aws_client('cloudformation')
        cloudformation.create_stack(
            StackName= self.name,
            TemplateURL= 'https://s3.amazonaws.com/aws-bigdata-blog/artifacts/AI_Face_Recognition_Service/1_RekognitionFaceDetection.yaml',
            Parameters=[{
                    'ParameterKey': 'BucketName',
                    'ParameterValue': self.bucket
                },
                {
                    'ParameterKey': 'DynamoDBTableName',
                    'ParameterValue': self.dynamo
                },
                {
                    'ParameterKey': 'CollectionName',
                    'ParameterValue': self.rekognition
                }
            ],
            Capabilities = ['CAPABILITY_IAM']
        )

    def get_aws_client(self, service):
        if self.aws_access_key is None or self.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")

        return boto3.client(service,
                            aws_access_key_id=self.aws_access_key,
                            aws_secret_access_key=self.aws_secret_key,
                            region_name=self.aws_region)

    def get_aws_session(self,service):
        if self.aws_access_key is None or self.aws_secret_key is None:
            raise exceptions.UserError("Configure Access Key and Access Secret")
        return boto3.Session(
            aws_access_key_id=self.aws_access_key,
            aws_secret_access_key=self.aws_secret_key,
            region_name=self.aws_region
        )


    def list_buckets(self):
        client = self.get_aws_client('s3')
        response = client.list_buckets()

        for bucket in response['Buckets']:
            os.system("echo '%s'" % (bucket["Name"]))


    def upload_images(self):
        images = [('images/camila.jpg', 'camila')
                  ]
        session= self.get_aws_session('s3')
        s3 = session.resource('s3')
        # Iterate through list to upload objects to S3
        for image in images:
            file = open(image[0], 'rb')
            object = s3.Object(self.bucket, 'index/' + image[0])
            ret = object.put(Body=file,
                             Metadata={'FullName': image[1]}
                             )

    def analizar(self):
        rekognition = self.get_aws_client('rekognition')
        dynamodb = self.get_aws_client('dynamodb')

        image = Image.open("images/seria.jpg")
        stream = io.BytesIO()
        image.save(stream, format="JPEG")
        image_binary = stream.getvalue()
        #image_binary = self.imagen

        response = rekognition.search_faces_by_image(
            CollectionId = self.rekognition,
            Image={'Bytes': image_binary}
        )

        for match in response['FaceMatches']:
            os.system("echo '%s'" % (match['Face']['FaceId']))
            os.system("echo '%s'" % (match['Face']['Confidence']))
            #print(match['Face']['FaceId'], match['Face']['Confidence'])

            face = dynamodb.get_item(
                TableName= self.dynamo,
                Key={'RekognitionId': {'S': match['Face']['FaceId']}}
            )

            if 'Item' in face:
                raise UserError(face['Item']['FullName']['S'])
                os.system("echo '%s'" % (face['Item']['FullName']['S']))
                #print(face['Item']['FullName']['S'])
            else:
                os.system("echo '%s'" % ("NO HAY"))
                raise UserError("no se encuentra")
                #print('no match found in person lookup')

    def multi(self):

        rekognition = self.get_aws_client('rekognition')
        dynamodb = self.get_aws_client('dynamodb')

        image = Image.open("images/grupo.jpg")
        stream = io.BytesIO()
        image.save(stream, format="JPEG")
        image_binary = stream.getvalue()

        response = rekognition.detect_faces(
            Image={'Bytes': image_binary}
        )

        all_faces = response['FaceDetails']

        # Initialize list object
        boxes = []

        # Get image diameters
        image_width = image.size[0]
        image_height = image.size[1]

        # Crop face from image
        for face in all_faces:
            box = face['BoundingBox']
            x1 = int(box['Left'] * image_width) * 0.9
            y1 = int(box['Top'] * image_height) * 0.9
            x2 = int(box['Left'] * image_width + box['Width'] * image_width) * 1.10
            y2 = int(box['Top'] * image_height + box['Height'] * image_height) * 1.10
            image_crop = image.crop((x1, y1, x2, y2))

            stream = io.BytesIO()
            image_crop.save(stream, format="JPEG")
            image_crop_binary = stream.getvalue()

            # Submit individually cropped image to Amazon Rekognition
            response = rekognition.search_faces_by_image(
                CollectionId= self.rekognition,
                Image={'Bytes': image_crop_binary}
            )

            if len(response['FaceMatches']) > 0:
                # Return results
                #print('Coordinates ', box)
                os.system("echo '%s'" % (box))
                for match in response['FaceMatches']:

                    face = dynamodb.get_item(
                        TableName= self.dynamo,
                        Key={'RekognitionId': {'S': match['Face']['FaceId']}}
                    )

                    if 'Item' in face:
                        person = face['Item']['FullName']['S']
                    else:
                        person = 'no match found'

                    #print(match['Face']['FaceId'], match['Face']['Confidence'], person)
                    os.system("echo '%s'" % (match['Face']['FaceId']))
                    os.system("echo '%s'" % (match['Face']['Confidence']))
                    os.system("echo '%s'" % (person))



