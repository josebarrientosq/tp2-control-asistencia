# -*- coding: utf-8 -*-

from . import models
from . import awsrekognition
from . import res_company
from . import hr_employee
from . import asistencia_marcas
from . import asistencia_zona