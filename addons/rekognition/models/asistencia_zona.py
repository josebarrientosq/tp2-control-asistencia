from odoo import models, fields, api

class Asistencia_zona(models.Model):
    _name = 'asistencia.zona'
    _description = 'zona de marcacion'

    name = fields.Char('Zona', required=True)
    descripcion = fields.Text('Descripcion')
    admin_ids = fields.Many2many('hr.employee','zona_admin_rel','zona_id', 'admin_id' , string='Administradores')
    empleados_ids = fields.Many2many('hr.employee', 'zona_employee_rel', 'zona_id', 'emp_id', string='Empleados')
    active = fields.Boolean('Activo', default=True)