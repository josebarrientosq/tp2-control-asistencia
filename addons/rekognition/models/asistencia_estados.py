from odoo import models, fields, api
from odoo.osv import expression


class AttendanceState(models.Model):
    _name = 'asistencia.estado'

    name = fields.Char(string='Nombre', required=True)
    code = fields.Integer(string='Codigo', required=True)
    type = fields.Selection([('entrada', 'Entrada'),
                            ('salida', 'Salida')], string='tipo', required=True)

    hora_cambio = fields.Float("Hora Cambio" ,required=True)

