# -*- coding: utf-8 -*-
{
    'name': "hr_asistencia",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','resource','hr_holidays','hr_holidays_public','to_base','rekognition'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/hr.holidays.status.xml',
        'views/templates.xml',
        'views/marca_view.xml',
        'views/resource_calendar_views.xml',
        'views/asistencia_mtpe_views.xml',
        'views/asistencia_estado_views.xml',
        'views/asistencia_turno_views.xml',
        'views/asistencia_zonas_view.xml',
        'wizard/asistencia_mtpe_wizard.xml'


    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}