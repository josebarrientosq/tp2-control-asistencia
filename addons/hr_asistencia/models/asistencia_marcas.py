from odoo import models, fields, api, _
from datetime import datetime

class marcas(models.Model):
    _inherit = 'asistencia.marca'

    asistencia_estado_id = fields.Many2one('asistencia.estado', compute='set_estado')
    turno_id = fields.Many2one('asistencia.turno', related='asistencia_estado_id.turno_id', store=True,
                                  index=True)
    tipo = fields.Selection([('entrada', 'Entrada'),('salida', 'Salida')], string='tipo', related='asistencia_estado_id.tipo',store=True)

    marca_mtpe_id = fields.Many2one('asistencia.mtpe')


    @api.one
    def set_estado(self):
        start_dt = datetime.strptime(str(self.marca), "%Y-%m-%d %H:%M:%S")
        tiempo = start_dt.time()
        hora = tiempo.hour + tiempo.minute / 60

        for zona_linea in self.zona_id.asistencia_estado_linea_id:
            if hora >= zona_linea.hora_cambio:
                self.asistencia_estado_id = zona_linea.asistencia_estado_id
