from odoo import models, fields, api
from odoo.osv import expression


class AttendanceState(models.Model):
    _name = 'asistencia.estado'
    _description = 'estados asistencia'

    name = fields.Char(string='Name', required=True, translate=True,
                       track_visibility='onchange')
    turno_id = fields.Many2one('asistencia.turno', string='Turno', required=True,
                                  help='Turno, e.j. Mañana, Tarde, Noche , horas extras ,etc', track_visibility='onchange')
    code = fields.Integer(string='Code Number', required=True, track_visibility='onchange')
    tipo = fields.Selection([('entrada', 'Entrada'),('salida', 'Salida')], string='tipo', required=True, track_visibility='onchange')

    hora_cambio = fields.Float("Hora Cambio" ,required=True)

    _sql_constraints = [
        ('code_unique',
         'UNIQUE(code)',
         "The Code must be unique!"),
        ('name_activity_id_unique',
         'UNIQUE(name)',
         "The state name must be unique within the same activity!"),
        ('name_activity_id_unique',
         'UNIQUE(tipo)',
         "The Activity Type and Activity must be unique! Please recheck if you have previously defined an attendance status with the same Activity Type and Activity"),
    ]


