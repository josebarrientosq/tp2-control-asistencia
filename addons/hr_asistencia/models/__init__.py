# -*- coding: utf-8 -*-

from . import resource_calendar_attendance
from . import asistencia_mtpe
from . import asistencia_estado
from . import asistencia_marcas
from . import asistencia_turno
from . import asistencia_zona
