from odoo import models, fields, api


class AttendanceActivity(models.Model):
    _name = 'asistencia.turno'
    _description = 'Turnos'

    code = fields.Char(string='Codigo', required=True,
                          track_visibility='onchange')
    name = fields.Char(string='Name', required=True, translate=True,
                              help='Nombre del turnoy. E.j. Mañana , tarde ,noche, horas extras, etc')

    asistencia_estado_ids = fields.One2many('asistencia.estado', 'turno_id', string='Estado')


    _sql_constraints = [
        ('unique_name',
         'UNIQUE(name)',
         "Nombre unico"),
    ]

