from odoo import models, fields, api, _
from datetime import datetime

class marcas(models.Model):
    _inherit = 'asistencia.zona'

    asistencia_estado_linea_id = fields.One2many('asistencia.zona.linea','zona_id', required=False, index=True)



class Asistencia_zona_estado_linea(models.Model):
    _name = 'asistencia.zona.linea'
    _description = 'linea zona estado'

    zona_id = fields.Many2one('asistencia.zona', string='zona', required=True, ondelete='cascade', index=True,copy=False)

    asistencia_estado_id = fields.Many2one('asistencia.estado', string='Estado', required=True, index=True)
    code = fields.Integer(string='Codigo', related='asistencia_estado_id.code', store=True, readonly=True)
    name = fields.Char('Nombre', related='asistencia_estado_id.name')
    tipo = fields.Selection([('ENTRADA', 'ENTRADA'),('SALIDA', 'SALIDA')], related='asistencia_estado_id.tipo',
                            store=True, readonly=True, index=True)
    turno_id = fields.Many2one('asistencia.turno', related='asistencia_estado_id.turno_id',
                                  help='Turno', readonly=True, store=True, index=True)
    hora_cambio = fields.Float("Hora Cambio" , related='asistencia_estado_id.hora_cambio')