# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions,_


class ResourceCalendarAttendance(models.Model):
    _inherit = "resource.calendar.attendance"

    turno_id = fields.Many2one('asistencia.turno', string='turno')
