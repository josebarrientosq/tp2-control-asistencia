# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions,_


class ResourceCalendarAttendance(models.Model):
    _inherit = "resource.calendar.attendance"

    turno_id = fields.Many2one('asistencia.turno', string='turno',
                                  help='This field is to group attendance into multiple Activity (e.g. Overtime, Normal Working, etc)')


