from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
import datetime
from datetime import datetime, date, timedelta , time
import os


class AsistenciaWizard(models.TransientModel):
    _name = 'asistencia.wizard.mtpe'
    _description = 'Asistencia Wizard'
    _inherit = ['to.base']

    fecha_inicio = fields.Date('Fecha inicio')
    fecha_fin = fields.Date('Fecha fin')
    calendario_feriado = fields.Many2one('hr.holidays.public','Feriados')


    def generar_data(self):
        fecha_fin = datetime.strptime(str(self.fecha_fin), "%Y-%m-%d")
        fecha_inicio = datetime.strptime(str(self.fecha_inicio), "%Y-%m-%d")


        for n in range(int((fecha_fin - fecha_inicio).days)):
            fecha_buscar = fecha_inicio + timedelta(n)
            dia_feriado = self.calendario_feriado.is_public_holiday(fecha_buscar)

            if not dia_feriado:
                self.generar4(fecha_buscar)

            else:
                os.system("echo '%s'" % ("FERIADO"))
    @api.multi
    def generar4(self,pub_date):

        employee_ids = self.env['hr.employee'].search([('active','=','True')])  # todos
        turno_ids = self.env['asistencia.turno'].search([])
        usuario_marcas_ids = self.env['asistencia.marca']
        # calendar = self.env['resource.calendar']
        asistencia_mtpe = self.env['asistencia.mtpe']


        os.system("echo '%s'" % ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"))

        min_pub_date_time = datetime.combine(pub_date, time.min)
        max_pub_date_time = datetime.combine(pub_date, time.max)

        timestamp_min_str = min_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        timestamp_max_str = max_pub_date_time.strftime("%Y-%m-%d %H:%M:%S")
        posicion_dia = min_pub_date_time.weekday()
        os.system("echo '%s'" % (timestamp_min_str))
        os.system("echo '%s'" % (timestamp_max_str))
        os.system("echo '%s'" % (posicion_dia))

        fecha_calculo = self.fecha_local(timestamp_min_str)
        for employee in employee_ids:

            #if employee.contract_id.state == 'open':
            if employee:
                os.system("echo '%s'" % ("Empleado :"))
                os.system("echo '%s'" % (employee.name))

                for turno_id in turno_ids:
                    os.system("echo '%s'" % (turno_id.name))
                    calendar = employee.resource_calendar_id
                    calendar_attendance = self.env['resource.calendar.attendance'].search(
                        [('calendar_id', '=', calendar.id),
                         ('dayofweek', '=', posicion_dia),
                         ('turno_id', '=', turno_id.id)])
                    os.system("echo '%s'" % (calendar_attendance))

                    if calendar_attendance:
                        entrada_calendario = str(calendar_attendance.hour_from)
                        salida_calendario = str(calendar_attendance.hour_to)
                        os.system("echo '%s'" % (str(calendar_attendance.hour_from)))
                        os.system("echo '%s'" % (str(calendar_attendance.hour_to)))
                        os.system("echo '%s'" % (str(turno_id.id)))

                        user_attendances = usuario_marcas_ids.search(
                            [('employee_id', '=', employee.id),
                             ('marca', '>', timestamp_min_str),
                             ('marca', '<', timestamp_max_str),
                             ], order='marca')
                        entrada = ""
                        salida = ""

                        os.system("echo '%s'" % ("-------------marcas-----------------"))
                        os.system("echo '%s'" % (user_attendances))


                        marcas = [(4, line.id) for line in user_attendances]

                        os.system("echo '%s'" % (marcas))
                        checkin = []
                        checkout = []
                        entrada_str = ""
                        salida_str = ""
                        todaslasmarcas = ""
                        for marca in user_attendances:
                            todaslasmarcas += self.fecha_a_hora_str(marca.marca) + " "
                            os.system("echo '%s'" % (todaslasmarcas))
                            if marca.tipo == 'entrada':
                                checkin.append(marca)
                            if marca.tipo == 'salida':
                                checkout.append(marca)

                        num_checkin = len(checkin)
                        num_checkout = len(checkout)

                        if num_checkin > 0:
                            entrada = checkin[0].marca
                            entrada_str = self.fecha_a_hora_str(entrada)
                        if num_checkout > 0:
                            salida = checkout[num_checkout - 1].marca
                            salida_str = self.fecha_a_hora_str(salida)

                        asistencia_mtpe.create({
                            'fecha': timestamp_max_str,
                            'employee_id': employee.id,
                            'turno_id': turno_id.id,
                            'marcas_ids': marcas,
                            'entrada': entrada,
                            'salida': salida,

                        })


                    else:
                        os.system("echo '%s'" % ("no tiene horario ese dia"))



    def fecha_a_hora_str(self,fecha):
        fecha_local = self.convert_utc_time_to_tz(fecha,'America/Lima')
        fecha_obj = datetime.strptime(fecha_local, "%Y-%m-%d %H:%M:%S")
        return fecha_obj.strftime("%H:%M")

    def fecha_local(self,fecha):
        return self.convert_utc_time_to_tz(fecha,'America/Lima')

    def get_marcas(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([()])
        for asistencia in asistencia_mtpe:
            asistencia.get_marcas()

    def crear_asistencia(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([('falta', '=', True) ])
        for asistencia in asistencia_mtpe:
            asistencia.crear_asistencia()

    def crear_inasistencia(self):
        asistencia_mtpe = self.env['asistencia.mtpe'].search([('falta', '=', True) ])
        for asistencia in asistencia_mtpe:
            asistencia.crear_inasistencia()

#lines = [(0, 0, line) for line in self._get_payslip_lines(contract_ids, payslip.id)]
